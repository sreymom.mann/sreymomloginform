/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Form from './src/components/Form';

AppRegistry.registerComponent(appName, () => Form);
