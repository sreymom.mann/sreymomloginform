
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';

export default class LoginView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    }
  }

  onClickListener = (viewId) => {
    Alert.alert("Welcome", "Mr/Ms. "+viewId);
  }

  render() {
      const {username,password} = this.state;
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={require('../assets/reactlogo.png')}/>
        <View style={styles.Subcontainer}>
            <View style={styles.InputLabel}>
                <Text style={{color: '#979A9A',fontWeight: 'bold',fontSize:14}}>Username</Text>
            </View>
            <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                underlineColorAndroid='transparent'
                onChangeText={(username) => this.setState({username})}/>
            </View>
            <View style={styles.InputLabel}>
                <Text style={{color: '#979A9A',fontWeight: 'bold',fontSize:14}}>Password</Text>
            </View>
            <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                secureTextEntry={true}
                underlineColorAndroid='transparent'
                onChangeText={(password) => this.setState({password})}/>
            </View>
            <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.onClickListener(username)}>
            <Text style={styles.loginText}>Log In</Text>
            </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ECECEC',
  },
  Subcontainer: {
    paddingTop:30,
    paddingLeft:30,
    paddingRight:30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  logo: {
    marginTop: -100,
    width:100,
    height:100,
    marginBottom:20
  },
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#ECECEC',
      borderBottomWidth: 2,
      width:250,
      height:45,
      marginTop:-10,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  InputLabel: {
    marginTop:-20,
    width:250,
    height:45,
    flexDirection: 'row',
    alignItems:'center',
    color:'#C0C0C0'
  },
  inputs:{
      height:45,
      marginLeft:8,
      borderBottomColor: '#ECECEC',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:32,
    width:70,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    borderRadius:2,
    marginLeft:180
  },
  loginButton: {
    backgroundColor: "#5AC6EE",
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold'
  }
});